import { assert, expect } from './chai.config.js';
import { CASSANDRA_TEST_CONFIG } from './config.js';
import {
  cassandraDBHelperTestSuite,
  cassandraJsonHelperTestSuite,
  fileStorageHelperTestSuite
} from './helpers/index.js';
import { cassandraServiceTestSuite } from './services/index.js';
import { cassandraService } from '../src/services/index.js';

describe('CassandraSchemaParsing', () => {
  let service;

  before(async () => {
    const servicePromise = cassandraService.connect(CASSANDRA_TEST_CONFIG);
    await expect(servicePromise).to.be.not.rejected;
    service = await servicePromise;
    assert.isTrue(service.isConnected());
  });

  before(async () => {
    await expect(service._dbHelper._client.execute(`create type ${CASSANDRA_TEST_CONFIG.keyspace}.${CASSANDRA_TEST_CONFIG.type} (` +
      'birthday timestamp,' +
      'nationality text,' +
      'weight smallint,' +
      'height tinyint' +
      ');')).to.be.not.rejected;
  });

  before(async () => {
    await expect(service._dbHelper._client.execute(`create table ${CASSANDRA_TEST_CONFIG.keyspace}.${CASSANDRA_TEST_CONFIG.table} (` +
      'name text primary key,' +
      'city text,' +
      'emails set<text>,' +
      'id int,' +
      'info test_info,' +
      'marks list<int>,' +
      'parents map<text, text>' +
      ')' +
      'with caching = {\'keys\': \'ALL\', \'rows_per_partition\': \'NONE\'}' +
      'and compaction = {\'class\': \'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy\', \'max_threshold\': \'32\', \'min_threshold\': \'4\'}' +
      'and compression = {\'chunk_length_in_kb\': \'64\', \'class\': \'org.apache.cassandra.io.compress.LZ4Compressor\'}' +
      'and dclocal_read_repair_chance = 0.1;')).to.be.not.rejected;

    await expect(service._dbHelper._client.execute(`insert into ${CASSANDRA_TEST_CONFIG.keyspace}.${CASSANDRA_TEST_CONFIG.table}` +
      '(name, city, emails, id, info, marks, parents) ' +
      'values (\'vova\',' +
      ' \'{"city": "Lviv", "street": "Chornovola","house":59, "neig": {"a": "1", "b":2}}\',' +
      ' {\'213@gmail.com\', \'5212@gmail.com\'},' +
      ' 1,' +
      ' {birthday:740350800000,nationality:\'{"country": "New Zeeland", "zip": 235}\',weight:10,height:10},' +
      ' [1, 2, 5, 4],' +
      ' {\'father\': \'Vasia\', \'mother\': \'Sveta\'}' +
      ');'
    )).to.be.not.rejected;
  });

  describe('cassandraDBHelperTestSuite', () => cassandraDBHelperTestSuite(() => service._dbHelper));
  describe('cassandraJsonHelperTestSuite ', () => cassandraJsonHelperTestSuite(() => service._dbHelper));
  describe('fileStorageHelperTestSuite', fileStorageHelperTestSuite);
  describe('cassandraServiceTestSuite', () => cassandraServiceTestSuite(() => service));

  after(async () => {
    if (service) {
      await expect(service._dbHelper._client.execute(`drop table ${CASSANDRA_TEST_CONFIG.keyspace}.${CASSANDRA_TEST_CONFIG.table}`)).to.be.not.rejected;
    }
  });

  after(async () => {
    if (service) {
      await expect(service._dbHelper._client.execute(`drop type ${CASSANDRA_TEST_CONFIG.keyspace}.${CASSANDRA_TEST_CONFIG.type}`)).to.be.not.rejected;
    }
  });

  after(async () => {
    if (service) {
      await expect(service.disconnect()).to.be.not.rejected;
    }
  });
});
