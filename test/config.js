export const CASSANDRA_TEST_CONFIG = {
  host: 'localhost',
  port: 9042,
  user: 'cassandra',
  password: 'cassandra',
  localDataCenter: 'datacenter1',
  // keyspace for testing purpose
  keyspace: 'bs2',
  // table name to be used to create table
  table: 'test_users',
  // type name to be used to create udt
  type: 'test_info'
};

export const FILESTORAGE_TEST_CONFIG = {
  filename: 'result_test.json'
}
