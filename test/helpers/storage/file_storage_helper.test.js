import fs from 'fs';
import { saveToFile } from '../../../src/helpers/index.js';
import { assert, expect } from '../../chai.config.js';
import { FILESTORAGE_TEST_CONFIG } from '../../config.js';

export const fileStorageHelperTestSuite = () => {
  it('should save schema object', async () => {
    const testData = 'test data';
    expect(() => saveToFile(FILESTORAGE_TEST_CONFIG.filename, testData, () => {
      assert.isTrue(fs.existsSync(FILESTORAGE_TEST_CONFIG.filename));
      assert.equal(fs.readFileSync(FILESTORAGE_TEST_CONFIG.filename).toString(), testData);
      fs.unlinkSync(FILESTORAGE_TEST_CONFIG.filename);
    })).not.to.throw();
  });
};
