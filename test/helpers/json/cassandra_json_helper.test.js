import { cassandraTableToJsonSchema } from '../../../src/helpers/index.js';
import { assert, expect } from '../../chai.config.js';
import { CASSANDRA_TEST_CONFIG } from '../../config.js';

export const cassandraJsonHelperTestSuite = getValue => {
  let dbHelper;

  before(() => {
    dbHelper = getValue();
  })

  it('should return schema object', async () => {
    const table = dbHelper._client.metadata.getTable(CASSANDRA_TEST_CONFIG.keyspace, CASSANDRA_TEST_CONFIG.table);
    await expect(table).to.be.not.rejected;
    const row = dbHelper.getTableRow(CASSANDRA_TEST_CONFIG.keyspace, CASSANDRA_TEST_CONFIG.table);
    await expect(row).to.be.not.rejected;
    const value = cassandraTableToJsonSchema(await table, await row);
    assert.exists(value, 'schema object not exists');
    assert.isObject(value, 'schema object is not object');
    assert.hasAnyKeys(value, ['$schema', 'title', 'type', 'properties'], 'schema object doesn\'t have required fields');
  });
};
