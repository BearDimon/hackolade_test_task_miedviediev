import { assert } from '../../chai.config.js';
import { CASSANDRA_TEST_CONFIG } from '../../config.js';

export const cassandraDBHelperTestSuite = getValue => {
  let dbHelper;

  before(() => {
    dbHelper = getValue();
  })

  it('should return keyspace names', async () => {
    const result = await dbHelper.getKeyspacesNames();
    assert.exists(result, 'keyspaces not exists');
    assert.isNotEmpty(result, 'keyspaces are empty');
  });
  it('should return tables names', async () => {
    const result = await dbHelper.getTableNames(CASSANDRA_TEST_CONFIG.keyspace);
    assert.exists(result, 'table name not exists');
    assert.isArray(result);
    assert.isNotEmpty(result);
  });
  it('should return table meta', async () => {
    const result = await dbHelper.getTableMeta(CASSANDRA_TEST_CONFIG.keyspace, CASSANDRA_TEST_CONFIG.table);
    assert.exists(result, 'table meta not exists');
    assert.typeOf(result, 'object', 'table meta is not object');
    assert.containsAllKeys(result, ['name', 'columns'], 'table meta doesn`t contain required fields');
  });
  it('should return table row', async () => {
    const result = await dbHelper.getTableRow(CASSANDRA_TEST_CONFIG.keyspace, CASSANDRA_TEST_CONFIG.table);
    assert.exists(result, `${CASSANDRA_TEST_CONFIG.keyspace}.${CASSANDRA_TEST_CONFIG.table} row not exists`);
    assert.typeOf(result, 'object', 'table row is not object');
    assert.hasAllKeys(result, ['name', 'city', 'emails', 'id', 'info', 'marks', 'parents'], 'table row object is not valid');
  })
};
