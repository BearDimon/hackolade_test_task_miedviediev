import { assert, expect } from '../chai.config.js';
import { CASSANDRA_TEST_CONFIG } from '../config.js';

export const cassandraServiceTestSuite = getValue => {
  let service;

  before(() => {
    service = getValue();
  });

  it('should return keyspaces names', async () => {
    const result = service.getKeyspacesNames();
    await expect(result).to.be.not.rejected;
    const value = await result;
    assert.exists(value, 'keyspaces names not exists');
    assert.isArray(value, 'keyspaces names is not array');
    assert.isNotEmpty(value, 'keyspaces names is empty');
    expect(value)
      .to
      .include
      .members([CASSANDRA_TEST_CONFIG.keyspace], 'keyspace names doesn\'t include configured keyspace');
  });

  it('should return keyspace tables names', async () => {
    const result = service.getKeyspaceTableNames(CASSANDRA_TEST_CONFIG.keyspace);
    await expect(result).to.be.not.rejected;
    const value = await result;
    assert.exists(value, 'keyspace tables names not exits');
    assert.isArray(value, 'keyspace tables names is not array');
    assert.isNotEmpty(value, 'keyspace tables names is empty');
    expect(value)
      .to
      .include
      .members([CASSANDRA_TEST_CONFIG.table], 'keyspace tables names doesn\'t include configured table');
  });

  it('should return keyspace tables metas', async () => {
    const result = service.getKeyspaceMeta(CASSANDRA_TEST_CONFIG.keyspace);
    await expect(result).to.be.not.rejected;
    const value = await result;
    assert.exists(value, 'keyspace tables metas not exists');
    assert.isArray(value, 'keyspace tables metas is not array');
    assert.isNotEmpty(value, 'keyspace tables metas is empty');
  });

  it('should return row', async () => {
    const result = service.getTableRow(CASSANDRA_TEST_CONFIG.keyspace, CASSANDRA_TEST_CONFIG.table);
    await expect(result).to.be.not.rejected;
    const value = await result;
    assert.exists(value, 'keyspace tables metas not exists');
    assert.isObject(value, 'keyspace tables metas is not object');
    assert.hasAllKeys(value, ['name', 'city', 'emails', 'id', 'info', 'marks', 'parents'], 'keyspace table row is not valid');
  });

  it('should return json', async () => {
    const result = service.keyspaceTableSchemaToJsonValue(CASSANDRA_TEST_CONFIG.keyspace, CASSANDRA_TEST_CONFIG.table);
    await expect(result).to.be.not.rejected;
    const value = await result;
    assert.exists(value, 'json not exists');
    assert.isString(value, 'json not string');
    expect(() => {
      const json = JSON.parse(value);
      assert.isObject(json, 'parsed json is not object');
      assert.hasAnyKeys(json, ['$schema', 'title', 'properties', 'type'], 'parsed json is not valid');
    })
      .not
      .to
      .throw();
  });

  it('should return array of json', async () => {
    const result = service.keyspaceSchemaToJsonValue(CASSANDRA_TEST_CONFIG.keyspace);
    await expect(result).to.be.not.rejected;
    const value = await result;
    assert.exists(value, 'json array not exists');
    assert.isString(value, 'json array is not string');
    expect(() => {
      const json = JSON.parse(value);
      assert.isArray(json, 'parsed json array is not array');
      assert.isObject(json[0], 'parsed json first element is not object');
      assert.hasAnyKeys(json[0], ['$schema', 'title', 'properties', 'type']);
    })
      .not
      .to
      .throw();
  });
};
