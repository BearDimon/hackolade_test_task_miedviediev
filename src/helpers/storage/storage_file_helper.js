import fs from 'fs';
import { StorageFileError } from '../../errors/index.js';

export const saveToFile = (path, data, onSuccess) => {
  fs.writeFile(path, data, err => {
    if (err) {
      throw new StorageFileError(`Problem with saving json to file: ${err.message}`);
    }
    onSuccess(path, data);
  });
};
