import cassandra from 'cassandra-driver';
import { schema_types } from '../../models/index.js';
import { JsonSchemaArgumentError, JsonSchemaParsingError } from '../../errors/index.js';

const dbTypes = cassandra.types.dataTypes;

export const cassandraTableToJsonSchema = (table, row) => {
  try {
    const props = table.columns.reduce(
      (properties, column) => (
        {
          ...properties,
          [column.name]: dbTypeToJsonSchemaType(
            {
              name: table.name,
              row: row
            },
            {
              name: column.name,
              type: column.type
            })
        }), {});
    return ({
      $schema: 'http://json-schema.org/draft-04/schema#',
      type: schema_types.OBJECT,
      title: table.name,
      properties: props
    });
  } catch (err) {
    if (err instanceof JsonSchemaArgumentError) {
      throw err;
    }
    throw new JsonSchemaParsingError(`Table parsing error with message: ${err.message}`);
  }
};

const dbTypeToJsonSchemaType = (table, column, field = null) => {
  switch (column.type.code) {
    case dbTypes.int:
    case dbTypes.tinyint:
    case dbTypes.smallint:
    case dbTypes.float:
    case dbTypes.double:
    case dbTypes.decimal:
    case dbTypes.varint:
    case dbTypes.bigint:
      return { type: schema_types.NUMBER };
    case dbTypes.boolean:
      return { type: schema_types.BOOLEAN };
    case dbTypes.list:
    case dbTypes.tuple:
      return {
        type: schema_types.ARRAY,
        items: dbTypeToJsonSchemaType(table, {
          ...column,
          type: column.type.info
        }, field)
      };
    case dbTypes.set:
      return {
        type: schema_types.ARRAY,
        items: dbTypeToJsonSchemaType(table, {
          ...column,
          type: column.type.info
        }, field),
        uniqueItems: true
      };
    case dbTypes.udt:
      const props = column.type.info.fields.reduce(
        (properties, field) => (
          {
            ...properties,
            [field.name]: dbTypeToJsonSchemaType(
              table,
              {
                ...column,
                type: field.type
              },
              {
                name: field.name,
                type: field.type
              })
          }), {});
      return {
        type: schema_types.OBJECT,
        properties: props
      };
    case dbTypes.map:
    case dbTypes.text:
    case dbTypes.varchar:
      const value = getValueAndParse(table, column, field);
      if (!value || typeof value !== 'object') {
        return { type: schema_types.STRING };
      }
      return valueToJsonSchemaType(value);
    default:
      return { type: schema_types.STRING };
  }
};

const getValueAndParse = (table, column, field = null) => {
  const value = table.row?.get(column.name);
  if (value) {
    try {
      if (field) {
        return JSON.parse(value[field.name]);
      }
      return JSON.parse(value);
    } catch (err) {
      return value;
    }
  }
  // or we can throw exception because we can't determine accurate type without row
  // in this case we just let column be string (by default) if column type is text or varchar
  return value;
};

const valueToJsonSchemaType = value => {
  const type = typeof value;
  switch (type) {
    case 'number':
    case 'bigint':
      return { type: schema_types.NUMBER };
    case 'boolean':
      return { type: schema_types.BOOLEAN };
    case 'object':
      if (Array.isArray(value)) {
        const items = value.size ? valueToJsonSchemaType(value[0]) : { type: schema_types.STRING };
        if (new Set(value).size === value.size) {
          return {
            type: schema_types.ARRAY,
            items: items,
            uniqueItems: true
          };
        } else {
          return {
            type: schema_types.ARRAY,
            items: items
          };
        }
      }
      const props = Object.keys(value)
        .reduce((properties, property) => (
          {
            ...properties,
            [property]: valueToJsonSchemaType(value[property])
          }), {});
      return {
        type: schema_types.OBJECT,
        properties: props
      };
    case 'string':
    case 'symbol':
      return { type: schema_types.STRING };
    default:
      throw new JsonSchemaArgumentError(`Invalid object value type: ${type}`);
  }
};
