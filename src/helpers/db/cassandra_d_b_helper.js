import * as cassandra from 'cassandra-driver';
import { DataBaseError } from '../../errors/index.js';

export class CassandraDBHelper {
  constructor(options) {
    this._client = new cassandra.Client({
      localDataCenter: options.localDataCenter,
      contactPoints: [options.host],
      authProvider: new cassandra.auth.PlainTextAuthProvider(options.user, options.password),
      protocolOptions: { port: options.port }
    });
  }

  isConnected() {
    return this._client.connected;
  }

  async connect() {
    return await this._client.connect()
      .catch(err => {
        throw new DataBaseError(`Connection problem with message: ${err.message}`);
      });
  }

  async disconnect() {
    return await this._client.shutdown()
      .catch(err => {
        throw new DataBaseError(`Disconnect problem with message: ${err.message}`);
      });
  }

  async getKeyspacesNames() {
    const keyspaces = await this._client.execute(
      'select keyspace_name from system_schema.keyspaces where keyspace_name < \'system\' allow filtering;')
      .catch(err => {
        throw new DataBaseError(err.message);
      });
    return keyspaces.rows.map(row => row.keyspace_name);
  }

  async getTableNames(keyspace) {
    const tables = await this._client.execute(
      `SELECT table_name FROM system_schema.tables WHERE keyspace_name = '${keyspace}';`)
      .catch(err => {
        throw new DataBaseError(err.message);
      });
    return tables.rows.map(row => row.table_name);
  }

  async getTableMeta(keyspace, tableName) {
    return await this._client.metadata.getTable(keyspace, tableName)
      .catch(err => {
        throw new DataBaseError(err.message);
      });
  }

  async getTableRow(keyspace, tableName) {
    const rows = await this._client.execute(
      `SELECT * FROM ${keyspace}.${tableName};`)
      .catch(err => {
        throw new DataBaseError(err.message);
      });
    return rows.first();
  }
}
