export const CASSANDRA_CONFIG = {
  host: 'localhost',
  port: 9042,
  user: 'cassandra',
  password: 'cassandra',
  localDataCenter: 'datacenter1',
  //keyspace to get data from
  //if empty, console will ask to select
  keyspace: null,
  //table to get data from
  //if empty, console will ask to select
  table: null
};

export const FILESTORAGE_CONFIG = {
  filename: 'result.json'
}
