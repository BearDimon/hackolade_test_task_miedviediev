export const errorCodes = {
  DB_ERROR: 'database',
  JSON_SCHEMA_ARGUMENT_ERROR: 'json/schema/argument',
  JSON_SCHEMA_PARSING_ERROR: 'json/schema/parsing',
  STORAGE_FILE_ERROR: 'storage/file',
  SERVICE_CASSANDRA_TABLE_NOT_FOUND: 'service/cassandra/table'
};
