export const schema_types = {
    NULL: 'null',
    BOOLEAN: 'boolean',
    NUMBER: 'number',
    STRING: 'string',
    OBJECT: 'object',
    ARRAY: 'array'
};
