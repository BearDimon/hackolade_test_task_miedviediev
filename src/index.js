import cliSelect from 'cli-select';
import chalk from 'chalk';
import { CASSANDRA_CONFIG, FILESTORAGE_CONFIG } from './config.js';
import { cassandraService } from './services/index.js';
import { saveToFile } from './helpers/index.js';
import { BaseError } from './errors/index.js';

const selectConfig = {
  valueRenderer: (value, selected) => {
    if (selected) {
      return chalk.green(chalk.underline(value));
    }
    return value;
  }
};

let service;

try {
  service = await cassandraService.connect(CASSANDRA_CONFIG);

  console.clear();
  if (!CASSANDRA_CONFIG.keyspace) {
    const keyspaces = await service.getKeyspacesNames();
    console.log(chalk.yellow('>') + chalk.bold(` Select keyspace (ESC or Ctrl-C to skip): (default ${keyspaces[0]})`));
    try {
      const resolvedValue = await cliSelect({
        ...selectConfig,
        values: keyspaces
      });
      CASSANDRA_CONFIG.keyspace = resolvedValue.value;
    } catch (err) {
      CASSANDRA_CONFIG.keyspace = keyspaces[0];
    }
  }

  console.clear();
  if (!CASSANDRA_CONFIG.table) {
    const tables = await service.getKeyspaceTableNames(CASSANDRA_CONFIG.keyspace);
    console.log(chalk.yellow('>') + chalk.bold(` Select table (ESC or Ctrl-C to skip): (default will create for all})`));
    try {
      const resolvedValue = await cliSelect({
        ...selectConfig,
        values: tables
      });
      CASSANDRA_CONFIG.table = resolvedValue.value;
    } catch (err) {
    }
  }

  console.clear();

  const json = CASSANDRA_CONFIG.table
    ?
    await service.keyspaceTableSchemaToJsonValue(CASSANDRA_CONFIG.keyspace, CASSANDRA_CONFIG.table)
    :
    await service.keyspaceSchemaToJsonValue(CASSANDRA_CONFIG.keyspace);

  saveToFile(FILESTORAGE_CONFIG.filename, json, path => {
    console.log(chalk.greenBright(`JSON data is saved here: ${path}`));
  });
} catch (err) {
  console.log(chalk.redBright(`Error \r\n`));
  if (!(err instanceof BaseError)) {
    console.log(chalk.redBright('Unexpected error'));
  }
  console.log(chalk.redBright(err.stack));
} finally {
  if (service?.isConnected()) {
    await service.disconnect();
  }
}
