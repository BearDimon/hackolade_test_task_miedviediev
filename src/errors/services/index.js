import { BaseError } from '../BaseError.js';
import { errorCodes } from '../../models/index.js';

export class CassandraTableNotFoundError extends BaseError {
  constructor(name, description = 'Table not exist in keyspace',
    errorCode = errorCodes.SERVICE_CASSANDRA_TABLE_NOT_FOUND) {
    super(name, errorCode, description);
  }
}
