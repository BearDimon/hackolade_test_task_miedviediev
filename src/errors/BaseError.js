export class BaseError extends Error {
  constructor (name, errorCode, description) {
    super(description)

    Object.setPrototypeOf(this, new.target.prototype)
    this.name = name;
    this.errorCode = errorCode
    Error.captureStackTrace(this)
  }
}
