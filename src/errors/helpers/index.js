import { BaseError } from '../BaseError.js';
import { errorCodes } from '../../models/index.js';

export class DataBaseError extends BaseError {
  constructor(name, description = 'Database error, please check for valid data',
    errorCode = errorCodes.DB_ERROR) {
    super(name, errorCode, description);
  }
}

export class JsonSchemaArgumentError extends BaseError {
  constructor(name, description = 'Argument error, please check for valid value',
    errorCode = errorCodes.JSON_SCHEMA_ARGUMENT_ERROR) {
    super(name, errorCode, description);
  }
}

export class JsonSchemaParsingError extends BaseError {
  constructor(name, description = 'Parsing error, please check for valid parse value',
    errorCode = errorCodes.JSON_SCHEMA_PARSING_ERROR) {
    super(name, errorCode, description);
  }
}

export class StorageFileError extends BaseError {
  constructor(name, description = 'File storage, please check config',
    errorCode = errorCodes.STORAGE_FILE_ERROR) {
    super(name, errorCode, description);
  }
}
