import { CassandraDBHelper, cassandraTableToJsonSchema } from '../helpers/index.js';
import { CassandraTableNotFoundError } from '../errors/index.js';

export const cassandraService = {
  connect: async (options) => {
    const service = new ConnectedService(new CassandraDBHelper(options));
    await service._dbHelper.connect();
    return service;
  }
};

class ConnectedService {
  constructor(dbHelper) {
    this._dbHelper = dbHelper;
  }

  isConnected() {
    return this._dbHelper.isConnected();
  }

  async getKeyspacesNames() {
    return await this._dbHelper.getKeyspacesNames();
  };

  async getKeyspaceTableNames(keyspace) {
    return await this._dbHelper.getTableNames(keyspace);
  };

  async getKeyspaceMeta(keyspace) {
    const tableNames = await this._dbHelper.getTableNames(keyspace);
    const tableMetasPromises = tableNames.map(async tableName => await this._dbHelper.getTableMeta(keyspace, tableName));
    return await Promise.all(tableMetasPromises);
  };

  async keyspaceSchemaToJsonValue(keyspace) {
    const tableMetas = await this.getKeyspaceMeta(keyspace);
    const schemaObjPromises = tableMetas.map(async tableMeta => {
      const row = await this.getTableRow(keyspace, tableMeta.name);
      return cassandraTableToJsonSchema(tableMeta, row);
    });
    const schemaObj = await Promise.all(schemaObjPromises);
    return JSON.stringify(schemaObj, null, 4);
  };

  async keyspaceTableSchemaToJsonValue(keyspace, tableName) {
    const tablesMeta = await this.getKeyspaceMeta(keyspace);
    const tableMeta = tablesMeta.find(table => table.name === tableName);
    if (!tableMeta) {
      throw new CassandraTableNotFoundError(`Table ${tableName} not exist in ${keyspace} keyspace`);
    }
    const row = await this.getTableRow(keyspace, tableMeta.name);
    const schemaObj = cassandraTableToJsonSchema(tableMeta, row);
    return JSON.stringify(schemaObj, null, 4);
  };

  async getTableRow(keyspace, tableName) {
    return await this._dbHelper.getTableRow(keyspace, tableName);
  };

  async disconnect() {
    await this._dbHelper.disconnect();
  }

}

